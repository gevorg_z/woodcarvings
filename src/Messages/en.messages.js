import LocalizedStrings from 'react-localization';

let messages = new LocalizedStrings({
    ru: {
        products : 'ПРОДУКЦИЯ',
        product : 'ПРОДУКТ',
        gallery : 'ГАЛЕРЕЯ',
        design_gallery: 'ГАЛЕРЕЯ',
        service : 'УСЛУГИ',
        about_us : 'О НАС',
        contact_us : 'СВЯЗЬ',
        information: 'ИНФОРМАЦИЯ',
        our_mission: 'Наша цель',
        loading: 'Загрузка...',
        interiors: 'ИНТЕРЬЕР',
        furniture: 'МЕБЕЛЬ',
        sketches: 'ЭСКИЗЫ',
        interior_design: 'Дизайн интерьера',
        interior_design_upper: 'ДИЗАЙН ИНТЕРЬЕРА',
        wood_carvings: 'Резьба по дереву',
        woodcarvings_upper: 'РЕЗНОЙ ДЕКОР ИЗ ДЕРЕВА',
        three_modeling: '3D моделирование',
        three_modeling_upper: '3D МОДЕЛИРОВАНИЕ НА ЗАКАЗ',
        decorating: 'Монтаж декора',
        decorating_upper: 'МОНТАЖ',
        mission_description: 'Предоставить вам качественный и надежный продукт',
        street : '21 Пионерская улица Москва,Россия',
        number : '+37455255265, +37496155265',
        mail : 'ligartdecor@gmail.com',
        subscribe : 'Подписаться на свежие новости',
        subscribe_footer: 'Подписаться',
        name_placeholder: 'Имя',
        how_to_order: 'Как заказать',
        how_to_order_upper: 'КАК ЗАКАЗАТЬ',
        how_to_pay: 'Оплата',
        how_to_pay_upper: 'ОПЛАТА',
        delivery: 'Доставка',
        delivery_upper: 'ДОСТАВКА',
        search: '\uD83D\uDD0D Найти',
        surname_placeholder: 'Фамили \nя',
        email_placeholder: 'Email',
        email_pristine : 'Поле электронной почты на может быть пустым',
        email_invalid : 'Адрес электронной почты неверный',
        message_input : 'Пожалуйста, введите ваше сообщение...',
        load_more: 'Загрузить еще',
        send: 'Отправить',
        find_us_upper: 'НАЙДИТЕ НАС',
        phone_us_upper: 'ПОЗВОНИТЕ НАМ',
        dimension : 'РАЗМЕРЫ (A x B x C) *',
        choose_dimension : 'Выберите размер...',
        add_to_cart : 'ДОБАВИТЬ В КОРЗИНУ',
        order : 'ЗАКАЗ',
        price : 'ЦЕНА',
        species : 'ТИП ДЕРЕВА *',
        count : 'КОЛ-ВО',
        choose_species : 'Выберите вид...',
        find_us: 'Найдите нас',
        our_novelties: 'НОВИНКИ',
        love_to_help: 'Нам нравится помогать вам!',
        oak : 'ДУБ',
        ash : 'ЯСЕНЬ',
        beech : 'БУК',
        categories : 'КАТЕГОРИИ',
        mouldings: 'Молдинги',
        mouldings_upper: 'МОЛДИНГИ',
        rosettes: 'Розетки',
        rosettes_upper: 'РОЗЕТКИ',
        balusters : 'Балясины',
        balusters_upper : 'БАЛЯСИНЫ',
        arches : 'Арки',
        arches_upper : 'АРКИ',
        baguettes : 'Багеты',
        baguettes_upper : 'БАГЕТЫ',
        bas_reliefs : 'Барельефы',
        bas_reliefs_upper : 'БАРЕЛЬЕФЫ',
        capitals : 'Капители',
        capitals_upper : 'КАПИТЕЛИ',
        pillars : 'Колонны',
        pillars_upper : 'КОЛОННЫ',
        mascarons : 'Маскароны',
        mascarons_upper: 'МАСКАРОНЫ',
        legs : 'Мебельные ножки',
        legs_upper : 'МЕБЕЛЬНЫЕ НОЖКИ',
        brackets : 'Кронштейны',
        brackets_upper : 'КРОНШТЕЙНЫ',
        souvenirs : 'Сувениры',
        souvenirs_upper : 'СУВЕНИРЫ',
        cartouche : 'Картуш',
        cartouche_upper : 'КАРТУШ',
        cornices_upper: 'КОРНИЗЫ',
        cornices: 'Корнизы',
        pilasters : 'Пилястры',
        pilasters_upper : 'ПИЛЯСТРЫ',
        doors : 'Двери',
        doors_upper : 'ДВЕРИ',
        panels : 'Панели',
        panels_upper : 'ПАНЕЛИ',
        decor : 'Декор',
        decor_upper : 'ДЕКОР',
        framework : 'Рамы',
        framework_upper : 'РАМЫ',
        appliques : 'Аппликации',
        appliques_upper : 'АППЛИКАЦИИ',
        portraits : 'Картины',
        portraits_upper : 'КАРТИНЫ',
        bars : 'Декоративные решетки',
        bars_upper : 'ДЕКОРАТИВНЫЕ РЕШЕТКИ',
        message_for_added_item: 'Деталь успешно довавлена в корзину.',
        message_for_order: 'Спасибо за заказ! В близжайщее время наш сотрудник свяжется с Вами.',
        register_order: 'Оформить заказ',
        cancel_button: 'Отмена',
        order_button: 'Заказать',
        phone_placeholder: 'Телефон',
        shopping_cart: 'КОРЗИНА',
        minimal_sum: 'Минимальная сумма заказа: 8000 RUB',
        order_sum: 'СУММА ЗАКАЗА: ',
        send_request: 'ОТПРАВИТЬ ЗАЯВКУ',
        contact_details: 'КОНТАКТНЫЕ ДАННЫЕ',
        contact_us_response: 'Спасибо за запрос! В близжайщее время наш сотрудник ответит Вам.',
        organization: 'Организация',
        private_person: 'Частное лицо',
        production: 'ПРОДУКЦИЯ',
        vendor_code: 'АРТИКУЛ',
        size: 'РАЗМЕР',
        material: 'МАТЕРИАЛ',
        contact_person: 'Контактное лицо',
        comment_for_order: 'Комментарий к заказу։',
        comments: 'Комментарий',
        search_results: 'РЕЗУЛЬТАТЫ ПОИСКА',
        subscribe_to_news: 'Спасибо за новостную подписку!',
        interior_design_text: 'Мы предлагаем выполнение дизайн-проекта интерьера c использованием нашего резного декора из дерева ' +
        'на основе \nВаших пожеланий.\n\n' +
        'Компания предоставляет полный спектр услуг – от разовых консультаций до разработки проекта и' +
        'воплощения \nего в жизнь.\n' +
        '\n' +
        'Визуализация интерьера.' +
        'Вы можете выбрать какой вид подачи дизайн-проекта Вам необходим. Мы можем \nпредоставить Вам чертеж, эскиз, чёрно-белую или фотореалистичную 3D визуализацию вашего интерьера с разных \nракурсов, с отделочными материалами, мебелью и освещением которые Вы выберите.\n' +
        '\n\nИсходные материалы, с которыми мы работаем:\n\n' +
        '1. Фотографии понравившегося Вам интерьера, декора или мебели;\n\n' +
        '2. Эскизы, наброски.\n\n' +
        '3. 3D модель.\n\n' +
        '4. Планировка, чертежи, развёртки стен.\n' +
        '\n' +
        'Выполнение заказа возможно, даже если необходимых материалов не хватает. В этом случае работа будет\n' +
        'выполнена на основе Ваших пожеланий.\n\n' +
        'Цена за каждый проект считается индивидуально и зависит от типа конкретной задачи, ее сложности и\n' +
        'требуемого времени.\n\n' +
        'Оценить уровень нашего профессионализма Bы можете по готовым работам, ознакомиться с которыми\n' +
        'можно в разделе «Галерея».\n' +
        '\n\n' +
        'По всем интересующим вас вопросам свяжитесь с нами по телефонам: +37455255265, +37496155265 или \nпо электронной ' +
        'почте: ligartdecor@gmail.com',
        decorating_text: 'Наша компания предлагаем Вам услуги профессионального монтажа нашего резного декора с резьбой по\n' +
        'дереву.\n\nКомпания гарантирует соблюдение сроков выполнения работ, а также учет интересов и пожеланий\n' +
        'заказчика в процессе выполнения. \n\nРаботаем быстро и качественно, выполняем полный спектр услуг от ' +
        'подготовки к ремонту и доставки \nматериалов, до полной реализации.\n\n\n\n' +
        'По всем интересующим вас вопросам свяжитесь с нами по телефонам: +37455255265, +37496155265 \nили по электронной ' +
        'почте: ligartdecor@gmail.com',
        threed_modeling_text: 'Наша компания предоставляет услуги по созданию 3D объектов любой сложности.\n' +
        '\nПри помощи технологии 3D моделирования можно воссоздать как реальный так и любой вымышленный\n' +
        'объект. Объектом трехмерной графики может быть все что угодно: интерьеры, мебель, модель декора и т.п.\n' +
        '3D визуализация позволяет увидеть предмет со всеми его деталями под любым углом до его воплощения в\n' +
        'жизни.\n' +
        '\nВы можете выбрать что именно Вам необходимо:\n\n'+
        '1.3D модель предмета или интерьера.\n\n' +
        '2.Упрощенная чёрно-белая 3D визуализация.\n\n' +
        '3.Фотореалистичная 3D визуализация.\n\n' +
        '\n' +
        'Исходные материалы, с которыми мы работаем:\n\n' +
        '1. Фотографии, эскизы, наброски.\n\n' +
        '2. Чертежи.\n\n' +
        '3. 3D модель Вашего объекта.\n\n' +
        'Цена за каждый проект считается индивидуально и зависит от типа конкретной задачи, ее сложности и\n' +
        'требуемого времени. Мы осуществляем 3D моделирование на заказ, в установленные сроки и с учетом всех\n' +
        'ваших пожеланий.\n\n' +
        'Ознакомиться с нашими работами Bы можете в разделе «Галерея».\n' +
        '\n\n' +
        'По всем интересующим вас вопросам свяжитесь с нами по телефонам: +37455255265, +37496155265 или \nпо электронной' +
        'почте: ligartdecor@gmail.com',
        cart_text: 'После получения оформленной корзины, мы свяжемся с Вами для подтверждения заказа и расскажем о дальнейших действиях.',
        about_us_text_p1: 'Компания занимается проектированием, и изготовлением высококачественных изделий резного декора и комплектующих для мебели,а так же создает уникальные дизайн-проэкты интереров с грамотным использыванием собственной продукции.',
        about_us_text_p2: 'Качество — самый главный критерий, которым мы руководствуемся в своем производстве, Мы предлагаем только качественную продукцию. Используемая древесина, отвечает строгим стандартам экологической безопасности.Резьба выполняется на высокотехнологичных станках с ЧПУ с дальнейшей ручной шлифовкой.Изделия проходят детальную дорезку и проработку внутренних граней для достижения высокой степени детализации, Все деревянные поверхности полируются и покрываются лаком или \n краской.',
        about_us_text_p3: 'Мы предоставляем клиентам широкий выбор готовых моделей резного декора и постоянно расширяем свой ассортимент, Несмотря на это, Наша команда профессионалов готова спроектировать и произвести нужную модель согласно Вашим пожеланиям, по вашим чертежам и эскизам. Наша компания предлагает производителям мебели и интерьеров декор по самым выгодным ценам! Мы выполняем заказы любой сложности!',
        woodcarvings_text: 'Мы предоставляем клиентам широкий выбор готовых моделей резного декора и постоянно расширяем\n' +
        'свой ассортимент.' +
        'Если Вы не нашли необходимый декор в нашей коллекции, наши дизайнеры готовы спроектировать\n и' +
        'произвести его согласно Вашим пожеланиям, по вашим чертежам и эскизам. Каждый заказ \n' +
        'тщательно продумывается и обсуждается с клиентом. Если у вас нет эскиза, наши дизайнеры создадут\n' +
        'его при помощи ваших подсказок, затем мастера создадут предмет непосредственно из массива дерева и\n' +
        'всего через несколько дней готовое изделие будет у вас. Наша команда профессионалов выполняет заказы\n' +
        'любой сложности!\n \n' +
        'В изготовлении резного декора используются разные породы дерева (дуб, ясень, бук и т.п.) Резьба\n' +
        'выполняется на высокотехнологичных станках с ЧПУ с дальнейшей ручной шлифовкой. Цветовое решение,\n' +
        'вид покрытия отделки изделий мы обязательно согласуем с Вами предварительно. Исходные материалы, с\n' +
        'которыми мы работаем:\n\n' +
        '1. Фотографии, эскизы, наброски.\n\n' +
        '2. Чертежи.\n\n' +
        '3. 3D модель Вашего объекта.\n' +
        '\n\n' +
        'По всем интересующим вас вопросам свяжитесь с нами по телефонам: +37455255265, +37496155265 или \nпо электронной ' +
        'почте: ligartdecor@gmail.com',
        how_to_order_text: 'Вы можете заказать нашу продукцию на сайте. \nВ разделе «Продукция» выберите понравившийся Вам товар,' +
        'положите его в виртуальную корзину и оформите заказ.\nПосле получения оформленной корзины, мы ' +
        'свяжемся с Вами для подтверждения заказа и расскажем о дальнейших \nдействиях.\n\nТакже вы можете ' +
        'заказать продукцию по электронной почте. Укажите в письме артикул изделия, материал, размер,\n' +
        'количество и Ваши контактные данные.Мы обязательно ответим Вам по телефону. Наши менеджеры ' +
        '\nпроконсультируют Вас и оформят заказ по телефону.\n\n\n' +
        'По всем интересующим вас вопросам свяжитесь с нами по телефонам: +37455255265, +37496155265 или \nпо электронной ' +
        'почте: ligartdecor@gmail.com',
        delivery_text: 'Доставка осуществляется по всей территории Российской Федерации. Вы можете заказать доставку\n' +
        'продукции по удобному для Вас адресу в любой город. \n\nСтоимость доставки зависит от габарита заказа и' +
        'удаленности места доставки. Стоимость рассчитают \nиндивидуально для Вас наши менеджеры.\n\nУслуги ' +
        'транспортной компании Вы можете самостоятельно оплатить при получении заказа либо включить в\n' +
        'стоимость заказа.'+ '\n'+ '\nСамовывоз' + '\n\n' + 'Вы можете самостоятельно забрать ваш заказ по предварительной ' +
        'договоренности с нашим менеджером.\n' +
        '\n\n\n' +
        'По всем интересующим вас вопросам свяжитесь с нами по телефонам: +37455255265, +37496155265 или \nпо электронной ' +
        'почте: ligartdecor@gmail.com',
        how_to_pay_text: 'Данный раздел сайта находится на реконструкции. Приносим свои извинения за доставленные неудобства.\n\n\n' +
        'По всем интересующим вас вопросам свяжитесь с нами по телефонам: +37455255265, +37496155265, или \nпо электронной ' +
        'почте: ligartdecor@gmail.com',
        numbers: 'RUS: Вскоре будет доступно \n' +
            'ARM: +37455255265, +37496155265'
    },

    en:{
        products : 'PRODUCTS',
        product : 'PRODUCT',
        gallery : 'GALLERY',
        service : 'SERVICE',
        about_us : 'ABOUT US',
        contact_us : 'CONTACT US',
        information: 'INFORMATION',
        our_mission: 'OUR MISSION',
        interiors: 'INTERIORS',
        furniture: 'FURNITURE',
        sketches: 'SKETCHES',
        interior_design: 'Interior design',
        interior_design_upper: 'INTERIOR DESIGN',
        wood_carvings: 'Wood carvings',
        woodcarvings_upper: 'CARVED WOOD DECORATION',
        three_modeling: '3D modelling',
        three_modeling_upper: '3D MODELLING AND VISUALIZATION',
        decorating: 'Decorating',
        decorating_upper: 'DECORATING',
        loading: 'Loading...',
        how_to_order: 'How to order',
        how_to_order_upper: 'HOW TO ORDER',
        how_to_pay: 'How to pay',
        how_to_pay_upper: 'HOW TO PAY',
        delivery: 'Delivery',
        delivery_upper: 'DELIVERY',
        search: '\uD83D\uDD0D Search',
        mission_description: 'To provide you with a quality and reliable product',
        street : '21 Pionerskaya Street Moscow,Russia',
        number : '+37455255265, +37496155265',
        mail : 'ligartdecor@gmail.com',
        load_more: 'Load more',
        subscribe: 'Subscribe to our latest news',
        subscribe_footer: 'Subscribe',
        find_us: 'Find us',
        find_us_upper: 'FIND US',
        phone_us_upper: 'PHONE US',
        our_novelties: 'NOVELTIES',
        order : 'ORDER',
        price : 'PRICE',
        love_to_help: 'We\'d love to help you!',
        name_placeholder: 'Name',
        contact_person: 'Contact person',
        contact_us_response: 'Thank you for the request! Our representative will answer you.',
        surname_placeholder: 'Last name',
        email_placeholder: 'Email',
        email_pristine: 'Email field can\'t be empty',
        email_invalid: 'Email is invalid',
        message_input : 'Please,enter your message here...',
        send : 'Send',
        dimension : 'DIMENSIONS ( A x B x C ) *',
        choose_dimension : 'Choose dimension...',
        count : 'COUNT',
        oak : 'OAK',
        ash : 'ASH',
        beech : 'BEECH',
        species : 'WOOD SPECIES *',
        choose_species : 'Choose specie...',
        categories: 'CATEGORIES',
        add_to_cart : 'ADD TO CART',
        balusters : 'Balusters',
        balusters_upper : 'BALUSTERS',
        arches : 'Arches',
        arches_upper: 'ARCHES',
        design_gallery: 'DESIGN GALLERY',
        baguettes : 'Baguettes',
        baguettes_upper : 'BAGUETTES',
        bas_reliefs : 'Bas-reliefs',
        bas_reliefs_upper: 'BAS-RELIEFS',
        capitals : 'Capitals',
        capitals_upper : 'CAPITALS',
        cornices_upper: 'CORNICES',
        cornices: 'Cornices',
        pillars : 'Pillars',
        pillars_upper: 'PILLARS',
        mascarons : 'Mascarons',
        mascarons_upper : 'MASCARONS',
        rosettes: 'Rosettes',
        rosettes_upper: 'ROSETTES',
        legs : 'Furniture legs',
        legs_upper : 'FURNITURE LEGS',
        brackets : 'Brackets',
        brackets_upper : 'BRACKETS',
        souvenirs : 'Souvenirs',
        souvenirs_upper : 'SOUVENIRS',
        cartouche : 'Cartouche',
        cartouche_upper : 'CARTOUCHE',
        pilasters : 'Pilasters',
        pilasters_upper : 'PILASTERS',
        doors : 'Doors',
        doors_upper : 'DOORS',
        panels : 'Panels',
        panels_upper : 'PANELS',
        decor : 'Decor',
        decor_upper : 'DECOR',
        framework : 'Frames',
        framework_upper : 'FRAMES',
        appliques : 'Appliques',
        appliques_upper : 'APPLIQUES',
        portraits : 'Paintings',
        portraits_upper : 'PAINTINGS',
        bars : 'Decorative gratings',
        bars_upper : 'DECORATIVE GRATINGS',
        mouldings: 'Mouldings',
        mouldings_upper: 'MOULDINGS',
        message_for_added_item: 'Item was successfully added to your cart.',
        register_order: 'Register an order',
        cancel_button: 'Cancel',
        order_button: 'Order',
        production: 'PRODUCTION',
        vendor_code: 'VENDOR CODE',
        size: 'SIZE',
        material: 'MATERIAL',
        phone_placeholder: 'Phone',
        shopping_cart: 'CART',
        minimal_sum: 'Minimal order sum: 8000 RUB',
        order_sum: 'ORDER SUM: ',
        contact_details: 'CONTACT DETAILS',
        send_request: 'SEND A REQUEST',
        organization: 'Organization',
        private_person: 'Private person',
        comment_for_order: 'Comment for order:',
        comments: 'Comments',
        search_results: 'SEARCH RESULTS',
        subscribe_to_news: 'Thank you for subscribing to our news!',
        message_for_order: 'Thank you for the order! Our representative will contact you.',
        interior_design_text: 'We offer the interior design services with the use of our carved wood decor based on your suggestions and wishes.\n\n' +
        'The company provides a full range of services - from one-time consultations to the development of the project and\n' +
        'its implementation.\n' +
        '\n' +
        'Interior visualization.You can choose the design visualization type that is required for your project. We can provide \nyou a blueprint' +
        ', sketch, black and white or photorealistic 3D visualization of your interior from different ' +
        'angles, \nwith the finishing materials, furniture and lighting that you select.\n\n' +
        'The raw materials with which we work are as follows:\n\n' +
        '1. Photos of the interior, decor and furniture you like.\n\n' +
        '2. Sketches.\n\n' +
        '3. 3D models.\n\n' +
        '4. Layouts, drawings, wall sweeps.\n\n' +
        'We carry out the order, even if the necessary materials are not enough. In this case, the work will be done on the\n' +
        'basis of your wishes.\n\n' +
        'The price for each project is calculated individually and depends on the type of the particular task, its complexity\n' +
        'and the required time.\n\n' +
        'To assess the level of our professionalism you can review our ready-made works, which can be found in the\n' +
        '\'Gallery\' section of the webpage.\n\n\n' +
        'If you have any questions, please, contact us by phones: +37455255265, +37496155265 or by E-mail: ligartdecor@gmail.com',
        decorating_text: 'Our company offers carved wood decor installation professional services.\n\n' +
        'In the process of project implementation, our team is always deadline oriented, we always take into consideration\n' +
        'your requests and interests.\n\n' +
        'We pride ourselves in delivering high-quality and fast projects. We provide a full range of services from\n' +
        'preparation to repair and delivery of materials, to full implementation.\n\n\n' +
        'If you have any questions, please, contact us by phones: +37455255265, +37496155265 or by E-mail: ligartdecor@gmail.com',
        cart_text: 'After we received your shopping cart request, we will contact you for order confirmation and will explain you the next steps',
        about_us_text_p1: 'Our Company designs and manufactures high-quality products of carved wood decor and components of furniture.Additionally, we create interior unique design projects with the usage of our own products.',
        about_us_text_p2: 'Quality is the most important criterion by which we are guided in our production and, therefore, we offer only\n' +
        'high-quality products. The wood used for the decor elements and ordered projects meets strict environmental\n' +
        'safety standards. The thread is made on high-tech CNC machines and is finalized with manual grinding. The\n' +
        'products undergo a detailed cutting and the elaboration of internal faces to achieve a high degree of detalization\n' +
        'and quality. All wooden surfaces are polished and covered with varnish or paint.',
        about_us_text_p3: 'We provide our customers a wide selection of ready-made models of carved wooden decors and are constantly\n' +
        'expanding their range. Furthermore, our professional team is ready to design and produce the desired model\n' +
        'according to your requirements, drawings, and sketches. We offer products to furniture manufacturers and\n' +
        'interior decor at the best prices. We implement orders of any complexity!',
        threed_modeling_text: 'Using 3D modeling technology, you can recreate both the real and any fictional object. The object of 3D  graphics\n' +
        'can be anything: interiors, furniture, a model of decor, and others. 3D visualization allows you to see the object\n' +
        'and all its details from any angle before it\'s brought to life.\n' +
        'You can select what exactly you need:\n\n' +
        '1. 3D model of an object or interior\n\n' +
        '2. Simplified black and white 3D visualization\n\n' +
        '3. Photorealistic 3D visualization\n\n' +
        'Raw materials we work with:\n\n' +
        '1. Photos, sketches\n\n' +
        '2. Blueprints.\n\n' +
        '3. 3D model of your object.\n\n' +
        'The price for each project is calculated individually and depends on the type of the particular task, it\'s complexity\n' +
        'and the required time. We carry out 3D modeling based on the order, within the discussed period of time and\n' +
        'taking into consideration all your requirements.\n\n' +
        'You can view our projects in the \'Gallery\' section of the webpage.\n\n\n' +
        'If you have any questions, please, contact us by phones: +37455255265, +37496155265 or by E-mail: ligartdecor@gmail.com',
        woodcarvings_text: 'We provide our customers a wide selection of ready-made models of carved decor and constantly expand our\n' +
        'product stock.\n \n' +
        'If you did not find the necessary decor in our collection, our designers can develop it according to your\n' +
        'requirements, drawings and sketches. Each order is thoroughly reviewed and discussed with the client. If you do\n' +
        'not have a sketch, our designers will create it with your help, then the professionals will create the object directly\n' +
        'from the wood and in just a few days you will have the finished product. Our team of professionals carries out\n' +
        'orders of any complexity!\n\n' +
        'In the manufacture of carved decor we use types of wood (oak, ash, beech, etc.). The carved decors are made on\n' +
        'high-tech CNC machines and later are finalized with manual grinding. We necessarily agree all the details of the\n' +
        'color solution, coating type of product finishing with you in advance.\n\n' +
        'Raw materials we work with:\n\n' +
        '1. Photos, sketches\n\n' +
        '2. Blueprints.\n\n' +
        '3. 3D model of your object.\n\n\n' +
        'If you have any questions, please, contact us by phones: +37455255265, +37496155265 or by E-mail: ligartdecor@gmail.com',
        how_to_order_text: 'You can order our products through our site. Under the “Products “ section choose the products you like, put them\n' +
        'in your basket and place your order. After receiving the order, our team members will contact you for order\n' +
        'confirmation and will provide you about further actions.\n\n' +
        'You can also order products by e-mail. Indicate in the email the article, material, size, quantity of the product you\n' +
        'want to order and please provide your contact information. We will certainly reply to you.\n\n' +
        'Additionally, you can also follow us through our socials. Our managers will consult you and will issue the order.\n\n\n' +
        'If you have any questions, please, contact us by phones: +37455255265, +37496155265 or by E-mail: ligartdecor@gmail.com',
        delivery_text: 'We organize Delivery throughout the whole  territory of the Russian Federation. You can order the delivery of\n' +
        'products at a convenient address for you in any city.\n\n' +
        'The cost of delivery depends on the size of the order and the delivery location. Our professional team of managers\n' +
        'will calculate the delivery price individually.\n\n' +
        'You can either pay for the product transportation directly to the transportation company or include it in the\n' +
        'product delivery price.\n\n' +
        'Pickup\n\n' +
        'In order to pick-up the order yourself, please contact our managers.\n\n\n' +
        'If you have any questions, please, contact us by phones: +37455255265, +37496155265 or by E-mail: ligartdecor@gmail.com',
        how_to_pay_text: 'The following page is under construction. We apologize for the inconvenience.\n\n\n' +
        'If you have any questions, please, contact us by phones: +37455255265, +37496155265 or by E-mail: ligartdecor@gmail.com',
        numbers: ' RUS: will be available soon' +
            'ARM: +37455255265, +37496155265'
    }
});

export default messages;