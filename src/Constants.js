var Constants = {
    SERVER_URL: 'https://woodcarvings-be.herokuapp.com/',
    AWS_URL: 'https://s3.eu-west-2.amazonaws.com/woocarvings/'
};
export default Constants;
