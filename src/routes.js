import React from 'react';
import {BrowserRouter, Route} from 'react-router-dom';
import App from './components/App';
import Contact from './components/Contact';
import About from './components/About';
import DesignGallery from './components/DesignGallery';
import CategoryWrapper from './components/Categories/CategoryWrapper';
import Order from './components/Order';
import Cart from './components/Cart';
import InteriorDesign from './components/Service/InteriorDesign';
import Decorating from './components/Service/Decorating';
import ThreeDModeling from './components/Service/ThreeDModeling';
import WoodCarvings from './components/Service/WoodCarvings';
import HowToOrder from './components/Information/HowToOrder';
import Delivery from './components/Information/Delivery';
import HowToPay from './components/Information/HowToPay';
import SearchResults from './components/SearchResults';



const Routes = (props) => (
  <BrowserRouter {...props}>
    <div>
        <Route exact path="/" component={App} />
        <Route exact path="/Design_gallery" component={DesignGallery} />
        <Route exact path="/Contact_us" component={Contact} />
        <Route exact path='/Categories/:category' component={CategoryWrapper}/>
        <Route exact path="/About_us" component={About} />
        <Route exact path="/Order/*" component={Order}/>
        <Route exact path="/Shopping_cart" component={Cart}/>
        <Route exact path="/Interior_design" component={InteriorDesign}/>
        <Route exact path="/Decorating" component={Decorating}/>
        <Route exact path="/3D_Modelling" component={ThreeDModeling}/>
        <Route exact path="/Wood_Carvings" component={WoodCarvings}/>
        <Route exact path="/How_to_order" component={HowToOrder}/>
        <Route exact path="/Delivery" component={Delivery}/>
        <Route exact path="/How_to_pay" component={HowToPay}/>
        <Route exact path="/Search_results" component={SearchResults}/>
    </div>
  </BrowserRouter>
);

export default Routes;

