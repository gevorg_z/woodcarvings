import Constants from '../Constants';

let requestManager = {
    makeRequest: (action, params, callback, method = 'GET') => {
        fetch(Constants.SERVER_URL + action + '?data=' + params, {
            method: method,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then((response) => {
            if (response.status === 200) {
                return response.json();
            } else return;
        }).then((body) => {
            callback(body);
        });
    },

    getNovelties: (callback) => {
        fetch(Constants.SERVER_URL + 'getNovelties', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then((response) => {
            if (response.status === 200) {
                return response.json();
            } else return;
        }).then((body) => {
            callback(body);
        });
    },

    serviceRequest: (callback, params, service, comment) => {
        let data = new FormData(params);
        data.append('service', service);
        data.append('comment', comment);
        fetch(Constants.SERVER_URL + 'serviceRequest', {
            method: 'POST',
            body: data
        }).then((response) => {
            if (response.status === 200) {
                return response.json();
            }
        }).then((body) => {
            callback(body);
        });
    }
};

export default requestManager;
