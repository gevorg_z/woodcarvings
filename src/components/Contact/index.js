import React from 'react';
import {ControlLabel, Button, FormControl, Grid} from 'react-bootstrap';
import Dialog from 'react-bootstrap-dialog';

import TopbarMain from '../App/TopbarMain';
import Footer from '../App/Footer';
import M from '../../Messages/en.messages';
import './styles.css'
import RequestManager from "../../ApiManager/RequestManager";

class Contact extends React.Component {
    constructor() {
        super();
        this.state = {
            name: '',
            surname: '',
            email: '',
            comment: '',
            nameIsEmpty: true,
            nameIsPristine: false,
            commentIsEmpty: true,
            commentIsPristine: false,
            emailIsEmpty: true,
            emailIsPristine: false,
            emailHasError: false
        };
        this.handleNameInput = this.handleNameInput.bind(this);
        this.handleEmailInput = this.handleEmailInput.bind(this);
        this.clickHandler = this.clickHandler.bind(this);
        this.handleCommentInput = this.handleCommentInput.bind(this);
    }

    componentDidMount() {
        window.addEventListener("language", function(e) {
            this.setState({});
        }.bind(this), false);
    }

    responseCallback = () => {
        this.dialog.showAlert(M.contact_us_response);
        this.setState({
            name: '',
            surname: '',
            email: '',
            comment: '',
            nameIsEmpty: true,
            nameIsPristine: false,
            commentIsEmpty: true,
            commentIsPristine: false,
            emailIsEmpty: true,
            emailIsPristine: false,
            emailHasError: false
        });
    };

    clickHandler(props) {
        switch (props.target.id) {
            case 'btn_sign_up':
                if (this.canBeSubmitted()) {
                    let params = {
                        name: this.state.name,
                        email: this.state.email,
                        comment: this.state.comment
                    };
                    RequestManager.makeRequest('contact', JSON.stringify(params), this.responseCallback, 'POST');
                }
                break;
            case 'btn_cancel':
                this.props.history.goBack();
                break;
            default:
                break;
        }
    }

    canBeSubmitted() {
        if (this.state.nameIsEmpty || this.state.commentIsEmpty ||
            this.state.emailIsEmpty) {
            return false;
        } else if (this.state.emailHasError) {
            return false;
        } else {
            return true;
        }
    }

    checkName() {
        if (this.state.name === '') {
            this.setState({
                nameIsEmpty: true
            });
            return false;
        } else {
            this.setState({
                nameIsEmpty: false
            });
            return true;
        }
    }

    checkComment() {
        if (this.state.comment === '') {
            this.setState({
                commentIsEmpty: true
            });
            return false;
        } else {
            this.setState({
                commentIsEmpty: false
            });
            return true;
        }
    }

    checkEmail() {
        const emailPattern = /(.+)@(.+){2,}\.(.+){2,}/;
        if (this.state.email === '') {
            this.setState({
                emailIsEmpty: true,
                emailHasError: false
            });
            return false;
        } else {
            this.setState({
                emailIsEmpty: false
            });
        }
        if (!emailPattern.test(this.state.email)) {
            this.setState({
                emailHasError: true
            });
            return false;
        } else {
            this.setState({
                emailHasError: false
            });
            return true;
        }
    }

    handleNameInput(event) {
        this.setState({
            name: event.target.value,
            nameIsPristine: true
        }, function () {
            this.checkName();
        });
    }

    handleEmailInput(event) {
        this.setState({
            email: event.target.value,
            emailIsPristine: true
        }, function () {
            this.checkEmail();
        });
    }

    handleCommentInput(event) {
        this.setState({
            comment: event.target.value,
            commentIsPristine: true
        }, function () {
            this.checkComment();
        });
    }


    render() {
        let isDisabled = false;
        if (this.state.nameIsEmpty || this.state.commentIsEmpty || this.state.emailHasError) {
            isDisabled = true;
        }
        return (
            <div>
                <TopbarMain/>
                <Grid className="register-main app">
                    <div className="register-group">
                        <div className="page-header">
                            <h2 className="signup-title">{M.contact_us}</h2>
                            <h3 className="signup-phrase">{M.love_to_help}</h3>
                        </div>
                        <div className="col-sm-offset-4">
                            <div className="register-block">
                                <FormControl
                                    className="register-control"
                                    placeholder={M.name_placeholder}
                                    onChange={this.handleNameInput}/>
                                <ControlLabel
                                    className="field-error"> {this.state.nameIsEmpty && this.state.nameIsPristine ? "Name field can't be empty" : null} </ControlLabel>
                            </div>
                            <div className="register-block">
                                <FormControl
                                    type="email"
                                    className="register-control"
                                    placeholder={M.email_placeholder}
                                    onChange={this.handleEmailInput}/>
                                <ControlLabel
                                    className="field-error"> {this.state.emailIsEmpty && this.state.emailIsPristine ? M.email_pristine : null} </ControlLabel>
                                <ControlLabel
                                    className="field-error"> {this.state.emailHasError ? M.email_invalid : null} </ControlLabel>
                            </div>
                            <div className="register-block">
                                <textarea id="text-block" onChange={this.handleCommentInput} rows="10" cols="45"
                                          name="text" placeholder={M.message_input}></textarea>
                            </div>
                        </div>
                        <div className="col-sm-offset-4">
                            <Button id="btn_sign_up" disabled={isDisabled} bsStyle="primary" type="submit"
                                    className="btn-register" onClick={this.clickHandler}>{M.send}</Button>
                        </div>
                        <div id="social">
                            <h3 id="find-us-contact"> {M.find_us_upper} </h3>
                            <i id="facebook-i" className="fa fa-facebook-square" aria-hidden="true"></i>
                            <i id="pinterest-i" className="fa fa-pinterest" aria-hidden="true"></i>
                            <a style={{color: 'unset'}} href="https://www.instagram.com/ligartdecor/" target="_blank">
                                <i id="instagram-i" className="fa fa-instagram" aria-hidden="true"></i>
                            </a>
                            <i id="youtube-i" className="fa fa-youtube" aria-hidden="true"></i>
                            <i id="vk-i" className="fa fa-vk" aria-hidden="true"></i>
                        </div>
                        <div id="phone">
                            <h3 id="phone-us">{M.phone_us_upper}</h3>
                            <div id="numbers">
                                <span id="number-all">{M.numbers}</span>
                            </div>
                        </div>
                    </div>
                </Grid>
                <Footer/>
                <Dialog ref={(el) => {
                    this.dialog = el
                }}/>
            </div>
        );
    }
}

export default Contact;

