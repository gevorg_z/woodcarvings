import React from 'react';
import {Button, Col, Grid, Row} from 'react-bootstrap';
import Dialog from 'react-bootstrap-dialog';

import './styles.css';

import Footer from "../App/Footer";
import TopbarMain from "../App/TopbarMain";

import RequestManager from "../../ApiManager/RequestManager";
import M from '../../Messages/en.messages';
import Constants from "../../Constants";

class Cart extends React.Component {
    constructor() {
        super();
        this.state ={
            person: 'private',
            phone : '',
            name: '',
            comment: '',
            email: ''
        }
    }

    componentDidMount() {
        window.addEventListener("language", function(e) {
            this.setState({});
        }.bind(this), false);
    }

    handleNameInput = (e) => {
        this.setState({
           name:e.target.value
        });
    };

    handlePhoneInput = (e) => {
        this.setState({
            phone: e.target.value
        });

    };

    handleEmailInput = (e) => {
        this.setState({
           email: e.target.value
        });
    };

    removeOrderItem(item) {
        for (let key in sessionStorage) {
            if (sessionStorage.hasOwnProperty(key)) {
                sessionStorage.removeItem(item);
                this.forceUpdate();
                break;
            }
        }
    }

    handleCommentChange = (e) => {
        this.setState({
           comment: e.target.value
        });
    };

    onRequestCallback = () => {
        this.dialog.showAlert(M.message_for_order);
    };

    sendRequest = () => {
        let order = {
            orderedItems: [],
            person: this.state.person,
            name: this.state.name,
            comment: this.state.comment,
            phone: this.state.phone,
            email: this.state.email
        };

        for (let key in sessionStorage) {
            if (sessionStorage.hasOwnProperty(key)) {
                let props = JSON.parse(sessionStorage[key]);
                let itemElement = {
                    code: props.code,
                    count: props.count,
                    dimension: props.dimension,
                    specie: props.specie
                };
                order.orderedItems.push(itemElement);
            }
        }
        RequestManager.makeRequest('confirmOrder', JSON.stringify(order), this.onRequestCallback, 'POST');
    };

    render(){
        let orderedItems = [];
        for (let key in sessionStorage) {
            if (sessionStorage.hasOwnProperty(key)) {
                let props = JSON.parse(sessionStorage[key]);
                let itemElement = (
                    <tr>
                        <td className="order-item-prop-image"><img src={Constants.AWS_URL + props.url}/></td>
                        <td className="order-item-prop">{props.code}</td>
                        <td className="order-item-prop">{props.count}</td>
                        <td className="order-item-prop">{props.dimension}</td>
                        <td className="order-item-prop">{props.specie}</td>
                        <td className="order-item-prop">
                            <i className="remove-order-item fa fa-times"
                               aria-hidden="true"
                               onClick={() => this.removeOrderItem(key.toString())}/>
                        </td>
                    </tr>

                );
                orderedItems.push(itemElement);
            }
        }
        const valid = !(this.state.phone && this.state.name && this.state.comment);
        return (
            <div>
                <TopbarMain/>
                <h3 id="design-gallery-title">{M.shopping_cart}</h3>
                <Grid className="app">
                    <Row>
                        <table id="order-table" className="col-sm-8 col-xs-12 col-sm-offset-2">
                            <tr id="order-table-header">
                                <th style={{paddingLeft: '1%'}}>{M.product}</th>
                                <th>{M.vendor_code}</th>
                                <th>{M.count}</th>
                                <th>{M.size}</th>
                                <th>{M.material}</th>
                                <th className="order-item-header-rm">RM</th>
                            </tr>
                            {orderedItems}
                        </table>
                    </Row>
                    <Row>
                        <div className="container-sum col-sm-8 col-sm-offset-2">
                        </div>
                    </Row>
                    <Row>
                        <h4 className="contact-details-title col-sm-offset-2 col-sm-6">{M.contact_details}</h4>
                    </Row>
                    <Row >
                        <Col className="contact-info col-sm-offset-1" sm={4 }>
                            <label className="radio-button col-sm-offset-3">
                                <input className="rorganization" type="radio" name="optradio"/>{M.organization}
                            </label>
                            <label className="radio-button col-sm-offset-3">
                                <input className ="rperson" type="radio" name="optradio" checked/>{M.private_person}
                            </label>
                            <input type="text"
                                className="cart-input col-sm-offset-3"
                                placeholder={M.contact_person}
                                onChange = {this.handleNameInput}/>
                            <input type="text"
                                className="cart-input col-sm-offset-3"
                                placeholder={M.phone_placeholder}
                                onChange={this.handlePhoneInput}/>
                            <input type="text"
                                className="cart-input col-sm-offset-3"
                                placeholder={M.email_placeholder}
                                onChange = {this.handleEmailInput}/>
                        </Col>
                        <Col className="comment-and-order" sm={6}>
                            <span className="comment-for-order col-sm-6">{M.comment_for_order}</span>
                            <textarea className="comment-area col-sm-4" onChange={this.handleCommentChange}></textarea>
                            <p className="cart-text col-sm-8">{M.cart_text}</p>
                            <Button className="send-request-button col-sm-3"
                                    disabled={valid}
                                    onClick={this.sendRequest}>{M.send_request}
                            </Button>
                        </Col>
                    </Row>
                    <Dialog ref={(el) => { this.dialog = el }} />
                </Grid >
                <Footer/>
            </div>
        )
    }
}

export default Cart;
