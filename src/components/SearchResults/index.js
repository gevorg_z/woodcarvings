import React from 'react';
import Footer from "../App/Footer";
import TopbarMain from '../App/TopbarMain';
import { Link } from 'react-router-dom';
import categoriesStructure from '../../CategoriesStructure';
import {ControlLabel, Grid} from "react-bootstrap";
import M from '../../Messages/en.messages';
import RequestManager from "../../ApiManager/RequestManager";
import "./styles.css";
import Constants from "../../Constants";


class SearchResults extends React.Component {
    constructor() {
        super();
        this.state = {
            categories: [],
            elements: [],
            imagesState: 1
        }
    }

    componentWillMount() {
        RequestManager.makeRequest('search', this.props.location.searchQuery, this.responseCallback);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.location.searchQuery !== this.props.location.searchQuery) {
            RequestManager.makeRequest('search', nextProps.location.searchQuery, this.responseCallback);
        }
    }

    componentDidMount() {
        window.addEventListener("language", function(e) {
            this.setState({});
        }.bind(this), false);
    }

    responseCallback = (elements) => {
        let categories = [];
        let categoriesList = categoriesStructure.eng;
        for (let category in categoriesList) {
            if (!!categoriesList[category].match(this.props.location.searchQuery)) {
                categories.push(categoriesList[category]);
            }
        }
        this.setState({
            categories: categories,
            elements: elements.result
        });
    };

    capitalizeFirstLetter = (word) => {
        return word.charAt(0).toUpperCase() + word.slice(1);
    };

    render() {
        let categories = this.state.categories.map((category) => {
            let categoryLink = '/Categories/' + this.capitalizeFirstLetter(category);
            return (
                <li className="category-litem">
                    <Link to={categoryLink}>
                        <ControlLabel className="category-item">
                            {M[category]}
                        </ControlLabel>
                    </Link>
                </li>);
        });
        let products = [];
        if (this.state.elements.length) {
            for (let i = 0, len = (this.state.imagesState * 16); i < len; ++i) {
                if (!this.state.elements[i]) {
                    break;
                }
                let source = Constants.AWS_URL + this.state.elements[i].pathcol,
                    order = "/Order/" + this.state.elements[i].codecol;
                products.push(
                    <Link to={order}>
                        <li className="list-item">
                            <img className="category-list-item" src={source}/>
                            <div className="category-info-container">
                                <span className="category-item-name">{this.state.elements[i].namecol}</span>
                                <span className="category-item-code">{this.state.elements[i].codecol}</span>
                            </div>
                        </li>
                    </Link>);
            }
        }
        return (
            <div>
                <TopbarMain/>
                <h3 id="design-gallery-title">{M.search_results}</h3>
                <Grid className="app">
                    <div className="search-category"> {M.categories} </div>
                    <ul>
                        {categories}
                    </ul>
                    <div className="separator"></div>
                    <div className="search-products"> {M.products}</div>
                    <ul>
                        {products}
                    </ul>
                </Grid>
                <Footer/>
            </div>
        );
    }
}

export default SearchResults;