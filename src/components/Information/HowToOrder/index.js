import React from 'react';
import Footer from "../../App/Footer";
import TopbarMain from '../../App/TopbarMain';
import M from '../../../Messages/en.messages';
import {Button, Grid} from "react-bootstrap";


class HowToOrder extends React.Component {

    componentDidMount() {
        window.addEventListener("language", function(e) {
            this.setState({});
        }.bind(this), false);
    }

    render() {
        return (
            <div>
                <TopbarMain/>
                <Grid className="app">
                    <h3 id="design-gallery-title">{M.how_to_order_upper}</h3>
                    <p className="information-text col-sm-offset-2">{M.how_to_order_text}</p>
                </Grid>
                <Footer/>
            </div>
        );
    }
}

export default HowToOrder;