import React from 'react';
import Footer from "../../App/Footer";
import TopbarMain from '../../App/TopbarMain';
import M from '../../../Messages/en.messages';
import {Button, Grid} from "react-bootstrap";


class Delivery extends React.Component {

    componentDidMount() {
        window.addEventListener("language", function(e) {
            this.setState({});
        }.bind(this), false);
    }

    render() {
        return (
            <div>
                <TopbarMain/>
                <Grid className="app">
                    <h3 id="design-gallery-title">{M.delivery_upper}</h3>
                    <p className="information-text col-sm-offset-2">{M.delivery_text}</p>
                </Grid>
                <Footer/>
            </div>
        );
    }
}

export default Delivery;