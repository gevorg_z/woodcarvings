import React from 'react';
import Carousel from 'react-image-carousel';

import './styles.css';

import Constants from "../../../Constants";

import '../../../../node_modules/react-image-carousel/lib/css/main.min.css';

class ImageCarousel extends React.Component {
    render() {
        let images = [];
        for (let image in this.props.images) {
            if (this.props.images[image]) {
                images.push(Constants.AWS_URL + this.props.images[image]);
            }
        }
        return (
            <div className="my-carousel">
                <Carousel images={images}
                          thumb={true}
                          loop={true}/>
            </div>
        );
    }
}

export default ImageCarousel;