import React from 'react';
import {Button, Grid, Row, Col} from 'react-bootstrap';
import Halogen from 'halogen';
import Dialog from 'react-bootstrap-dialog';

import './styles.css';
import ImageCarousel from "./Carousel/index";

import Footer from "../App/Footer";
import TopbarMain from '../App/TopbarMain';


import RequestManager from "../../ApiManager/RequestManager";
import M from '../../Messages/en.messages';

class Order extends React.Component {
    constructor() {
        super();
        this.state = {
            fileInformation: '',
            specie: '',
            dimension: '',
            count: 1,
            code: '',
            addToCartDisabled: false
        };
        this.responseCallback = this.responseCallback.bind(this);
    }

    componentWillMount() {
        let location = window.location.href.split('/'),
            itemCode = location[location.length - 1];
        this.setState({
            code: itemCode
        });
        RequestManager.makeRequest('order/', itemCode, this.responseCallback);

    }

    componentDidMount() {
        window.addEventListener("language", function(e) {
            this.setState({});
        }.bind(this), false);
    }

    responseCallback(response) {
        let sizes = response.result[0].sizecol;
        this.setState({
            fileInformation: response.result[0],
            sizes: sizes
        });
    }

    onSpeciesChoose(e) {
        this.setState({
            specie: e.target.value
        });
    }

    onDimensionChoose(e) {
        this.setState({
            dimension: e.target.value
        });
    }

    onCountChoose(e) {
        if (e.target.value < 1) {
            this.setState({
                addToCartDisabled: true
            });
        } else {
            this.setState({
                count: e.target.value,
                addToCartDisabled: false
            });
        }
    }

    addToCart() {
        sessionStorage.setItem(this.state.code,
            JSON.stringify({
                url: this.state.fileInformation.otherimagescol[0],
                count: this.state.count,
                specie: this.state.specie,
                code: this.state.code,
                dimension: this.state.dimension
            })
        );
        this.setState({addToCartDisabled: true}, () => {
            this.dialog.showAlert(M.message_for_added_item);
        });
    }

    render() {
        let jsxTemplate = (
            <div className="loader-container">
                <Halogen.DotLoader size="80px" color='black'/>
                <span className="loader-span">{M.loading}</span>
            </div>
        );
        if (this.state.fileInformation) {
            let tempSizes = [];
            let sizes = this.state.sizes.map((key) => {
                if (key && !tempSizes.includes(key)) {
                    tempSizes.push(key);
                    return (
                        <option>{key}</option>
                    );
                }
            });

            jsxTemplate = (
                <Grid className="app">
                    <Row>
                        <Col sm={6} xs={12}>
                            <ImageCarousel images={this.state.fileInformation.otherimagescol}/>
                        </Col>
                        <Col smOffset={1} sm={5} xs={12}>
                            <span>{this.state.code}</span>
                            <span className="selection">{M.species}</span>
                            <select id="detail-type" onChange={this.onSpeciesChoose.bind(this)}>
                                <option selected disabled>{M.choose_species}</option>
                                <option>{M.oak}</option>
                                <option>{M.beech}</option>
                                <option>{M.ash}</option>
                            </select>
                            <span className="selection">{M.dimension}</span>
                            <select id="detail-size" onChange={this.onDimensionChoose.bind(this)}>
                                <option selected disabled>{M.choose_dimension}</option>
                                {sizes}
                            </select>
                            <span className="selection">{M.count}</span>
                            <input className="count-input" type="number" defaultValue={this.state.count} onChange={this.onCountChoose.bind(this)}/>
                            <Button id="add-to-cart"
                                    bsStyle="primary"
                                    bsSize="large"
                                    disabled={this.state.addToCartDisabled || !(this.state.specie && this.state.dimension)}
                                    onClick={this.addToCart.bind(this)}>{M.add_to_cart}</Button>
                        </Col>
                    </Row>
                    <Dialog ref={(el) => {
                        this.dialog = el
                    }}/>
                </Grid>
            );
        }

        return (
            <div>
                <TopbarMain/>
                <h3 id="design-gallery-title">{M.order}</h3>
                {jsxTemplate}
                <Footer/>
            </div>
        );
    }
}

export default Order;
