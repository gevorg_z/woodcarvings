import React from 'react';
import {Button, Grid} from 'react-bootstrap';

import '../styles.css';
import ServiceModal from '../ServiceModal';

import Footer from '../../App/Footer';
import TopbarMain from '../../App/TopbarMain';

import M from '../../../Messages/en.messages';
import Constants from '../../../Constants';

class WoodCarvings extends React.Component {
    constructor() {
        super();
        this.state = {
            showModal: false
        };
    }

    componentDidMount() {
        window.addEventListener("language", function(e) {
            this.setState({});
        }.bind(this), false);
    }

    openRequestModal() {
        this.setState({
            showModal: true
        });
    }
    closeRequestModal() {
        this.setState({
            showModal: false
        });
    }

    render() {
        return (
            <div>
                <TopbarMain/>
                <Grid className="app">
                    <h3 id="design-gallery-title">{M.woodcarvings_upper}</h3>
                    <p className="information-text col-sm-offset-2">{M.woodcarvings_text}</p>
                    <div className="button-image-container">
                        <Button onClick={this.openRequestModal.bind(this)}
                                className="info-send-request-button">{M.send_request}</Button>
                        <img className="information-image" src={Constants.AWS_URL + 'carving.jpg'}/>
                    </div>
                </Grid>
                <Footer/>
                <ServiceModal service={M.wood_carvings} show={this.state.showModal} close={this.closeRequestModal.bind(this)}/>
            </div>
        );
    }
}

export default WoodCarvings;