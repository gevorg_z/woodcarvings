import React from 'react';
import {Button, Modal, Row, Col} from "react-bootstrap";

import './styles.css';

import M from '../../../Messages/en.messages';
import RequestManager from '../../../ApiManager/RequestManager';

class ServiceModal extends React.Component {
    constructor() {
        super();
        this.state = {
            person: '',
            phone: '',
            email: '',
            comment: '',
            file: ''
        }
    }

    responseCallback = (response) => {
        this.props.close();
    };

    handleNameInput = (e) => {
        this.setState({
            person: e.target.value
        });
    };

    handlePhoneInput = (e) => {
        this.setState({
            phone: e.target.value
        });
    };

    handleEmailInput = (e) => {
        this.setState({
            email: e.target.value
        });
    };

    handleCommentInput = (e) => {
        this.setState({
            comment: e.target.value
        });
    };

    handleFileInput = (e) => {
        this.setState({
            file: e.target.files[0]
        });
    };

    sendRequest = () => {
        RequestManager.serviceRequest(this.responseCallback, this.refs.uploadForm, this.props.service, this.state.comment);
    };

    render() {
        return (
            <Modal {...this.props} onHide={this.props.close}>
                <Modal.Header closeButton>
                    <Modal.Title>{this.props.service}</Modal.Title>
                </Modal.Header>
                <Modal.Body className="service-modal-body">
                    <form ref="uploadForm">
                        <Row>
                            <Col sm={6} xs={12} >
                                <input type="text"
                                       className="request-input"
                                       placeholder={M.contact_person}
                                       name="person"
                                       onChange = {this.handleNameInput}/>
                                <input type="text"
                                       className="request-input"
                                       name="phone"
                                       placeholder={M.phone_placeholder}
                                       onChange={this.handlePhoneInput}/>
                                <input type="text"
                                       className="request-input"
                                       name="email"
                                       placeholder={M.email_placeholder}
                                       onChange = {this.handleEmailInput}/>
                                <input className="upload-button"
                                       type="file"
                                       name="file"
                                       onChange={this.handleFileInput}/>
                            </Col>
                            <Col sm={6} xs={12} >
                                <span className="comment">{M.comments}</span>
                                <textarea style={{display: 'block'}} onChange={this.handleCommentInput}/>
                            </Col>
                        </Row>
                    </form>
                </Modal.Body>
                <Modal.Footer>
                    <Button bsStyle="custom"
                            disabled={!(this.state.person && this.state.phone && this.state.email && this.state.comment)}
                            onClick={this.sendRequest}>Send</Button>
                </Modal.Footer>

            </Modal>
        );
    }
}

export default ServiceModal;
