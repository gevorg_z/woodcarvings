/**
 * Created by gevorg on 8/7/17.
 */
import React from 'react';
import {Glyphicon, ControlLabel, FormControl, Button, Grid, Row, Col} from 'react-bootstrap';
import Dialog from 'react-bootstrap-dialog';
import M from '../../../Messages/en.messages';
import RequestManager from '../../../ApiManager/RequestManager';


import './styles.css';

class Footer extends React.Component {
    constructor() {
        super();
        this.state = {
            incorrectEmail: true,
            emailToSubscribe: ''
        };
        this.checkEmail = this.checkEmail.bind(this);
    }

    checkEmail(event) {
        const emailPattern = /(.+)@(.+){2,}\.(.+){2,}/;
        let email = event.target.value;
        this.setState({
            emailToSubscribe: email,
            incorrectEmail: !emailPattern.test(email)
        });
    }

    onSubscribeCallback() {
        this.dialog.showAlert(M.subscribe_to_news);
        this.setState({
            emailToSubscribe: ''
        });
    }

    subscribeEmail = () => {
        RequestManager.makeRequest('subscribe', this.state.emailToSubscribe, this.onSubscribeCallback.bind(this), 'POST');
    };

    render() {
        return (
            <Grid fluid id="footer">
                <Row>
                    <Col sm={4} className="footer-item footer-item-first">
                        <div>
                            <Glyphicon className="glyph-icon" glyph="map-marker"/>
                            <span className="footer-span">{M.street}</span>
                        </div>
                        <div>
                            <Glyphicon className="glyph-icon" glyph="earphone"/>
                            <span className="footer-span">{M.number}</span>
                        </div>
                        <div>
                            <Glyphicon className="glyph-icon" glyph="envelope"/>
                            <span className="footer-span">{M.mail}</span>
                        </div>
                    </Col>
                    <Col sm={4}>
                        <span id="subscribe-title" className="col-sm-12">
                            {M.subscribe}
                        </span>
                        <input
                            type="email"
                            ref="subscribeEmail"
                            className="subscribe-control"
                            placeholder="Email"
                            onChange={this.checkEmail}
                            value={this.state.emailToSubscribe}/>
                        <Button id="subscribe-button" disabled={this.state.incorrectEmail} onClick={this.subscribeEmail}>{M.subscribe_footer}</Button>
                    </Col>
                    <Col sm={4}>
                        <span id="find-us">
                            {M.find_us}
                        </span>
                        <div>
                            <i id="facebook-icon" className="fa fa-facebook-square" aria-hidden="true"/>
                            <i id="twitter-icon" className="fa fa-pinterest-square" aria-hidden="true"/>
                            <a href="https://www.instagram.com/ligartdecor/" target="_blank">
                                <i id="instagram-icon" className="fa fa-instagram" aria-hidden="true"/>
                            </a>
                            <i id="youtube-icon" className="fa fa-youtube" aria-hidden="true"/>
                            <i id="vk-icon" className="fa fa-vk" aria-hidden="true"/>
                        </div>
                    </Col>
                </Row>
                <Dialog ref={(el) => { this.dialog = el }} />
            </Grid>
        );
    }
}

export default Footer;