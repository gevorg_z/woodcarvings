import React from 'react';
import Carousel from 'nuka-carousel';
import {Grid, Col, Row} from "react-bootstrap";
import {Link} from 'react-router-dom';

import './styles.css';

import Topbar from '../Topbar';

import Constants from "../../../Constants";

import M from '../../../Messages/en.messages';

class MainTopbar extends React.Component {
    setLanguage = (language, e) => {
        M.setLanguage(language);
        const event = new Event("language");
        window.dispatchEvent(event);
    };

    render() {
        const logoSrc = Constants.AWS_URL + 'fonts.png';
        return (
            <div>
                <Grid fluid id="tops">
                    <Row>
                        <Col id="main-image-container" smOffset={3} sm={6}>
                            <Link id="home-page" to="/"><img className="logo-image" src={logoSrc}/></Link>
                        </Col>
                        <Col  id="localize-container" sm={3}>
                            <div>
                                <span className='localize-item' onClick={() => this.setLanguage('en')}>ENG</span>
                                <span className='localize-item' onClick={() => this.setLanguage('ru')}>RUS</span>
                            </div>
                        </Col>
                    </Row>
                </Grid>
                <Carousel autoplay={true} autoplayInterval={4000} wrapAround={true}>
                    <img style={{height: '300px'}} src={Constants.AWS_URL + '1c.jpg'}/>
                    <img style={{height: '300px'}} src={Constants.AWS_URL + '2c.jpg'}/>
                    <img style={{height: '300px'}} src={Constants.AWS_URL + '3c.jpg'}/>
                </Carousel>
                <Topbar/>
            </div>
        );
    }
}

export default MainTopbar;