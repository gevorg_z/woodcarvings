import React from 'react';
import './styles.css';
import TopbarMain from './TopbarMain'
import LastProducts from "./LastProducts";
import Footer from "./Footer";
import M from '../../Messages/en.messages';
import {Grid} from "react-bootstrap";

class App extends React.Component {
    componentDidMount() {
        window.addEventListener("language", function(e) {
            this.setState({});
        }.bind(this), false);
    }

    render() {
        return (
            <div>
                <TopbarMain/>
                <Grid id="last-products">
                    <h4 id="novelties">{M.our_novelties}</h4>
                    <LastProducts />
                </Grid>
                <Footer/>
            </div>
        );
    }

}

export default App;
