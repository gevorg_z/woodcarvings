import React from 'react';
import './styles.css';
import {Grid} from "react-bootstrap";
import RequestManager from "../../../ApiManager/RequestManager";
import Constants from "../../../Constants";
import {Link} from "react-router-dom";
import {CircleLoader} from 'react-spinners';



class LastProducts extends React.Component {
    constructor() {
        super();
        this.state = {
            novelties: []
        }
    }

    componentWillMount() {
        RequestManager.getNovelties(this.responseCallback);
    }

    responseCallback = (novelties) => {
        this.setState({
            novelties: novelties.result
        });
    };

    render() {
        let lastProducts = [];
        if (this.state.novelties.length) {
            lastProducts = this.state.novelties.map((noveltie) => {
                let source = Constants.AWS_URL + noveltie.pathcol,
                    order = "/Order/" + noveltie.codecol;
                return (
                    <Link to={order}>
                        <li className="list-item">
                            <img className="category-list-item" src={source}/>
                            <div className="last-product-info-container">
                                <span className="category-item-code">{noveltie.codecol}</span>
                            </div>
                        </li>
                    </Link>);
            });
        } else {
            lastProducts = (
                <div className="loader-container">
                    <CircleLoader color='black' loading={true} size={120}/>
                    <span className="loader-span">Loading...</span>
                </div>
            );
        }

        return (
            <Grid className="app">
                <ul id="images-list">
                    {lastProducts}
                </ul>
            </Grid>

        );
    }
}

export default LastProducts;


