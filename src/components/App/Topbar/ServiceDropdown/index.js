/**
 * Created by gevorg on 8/8/17.
 */
import React from 'react';
import {Link} from 'react-router-dom';
import {ControlLabel} from 'react-bootstrap';
import M from '../../../../Messages/en.messages';
import './styles.css';

class ServiceDropdown extends React.Component {
    render() {
        return (
            <div>
                <h3 id='dropdown-services-title'></h3>
                <div id="dropdown-services">
                    <div id="block1">
                        <Link to="/Interior_design"><ControlLabel
                            className="dropdown-item">{M.interior_design}</ControlLabel></Link>
                        <Link to="/Wood_Carvings"><ControlLabel
                            className="dropdown-item">{M.wood_carvings}</ControlLabel></Link>
                        <Link to="/3D_Modelling"><ControlLabel
                            className="dropdown-item">{M.three_modeling}</ControlLabel></Link>
                        <Link to="/Decorating"><ControlLabel
                            className="dropdown-item">{M.decorating}</ControlLabel></Link>
                    </div>
                </div>
            </div>
        );
    }
}

export default ServiceDropdown;

