/**
 * Created by gevorg on 8/8/17.
 */
import React from 'react';
import {Link} from 'react-router-dom';
import {ControlLabel} from 'react-bootstrap';
import M from '../../../../Messages/en.messages';
import './styles.css'

class InformationDropdown extends React.Component {
    render() {
        return (
            <div>
                <h3 id='dropdown-information-title'></h3>
                <div id="dropdown-information">
                    <div id="block1">
                        <Link to="/How_to_order"><ControlLabel className="dropdown-item">{M.how_to_order}</ControlLabel></Link>
                        <Link to="/How_to_pay"><ControlLabel
                            className="dropdown-item">{M.how_to_pay}</ControlLabel></Link>
                        <Link to="/Delivery"><ControlLabel className="dropdown-item">{M.delivery}</ControlLabel></Link>
                    </div>
                </div>
            </div>
        );
    }
}

export default InformationDropdown;

