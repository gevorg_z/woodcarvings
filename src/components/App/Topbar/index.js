import React from 'react';
import {NavItem, Navbar, Nav} from 'react-bootstrap';
import {Link, withRouter} from 'react-router-dom';
import M from '../../../Messages/en.messages';
import Dropdown from './Dropdown';
import ServiceDropdown from './ServiceDropdown';
import InformationDropdown from './InformationDropdown';
import './styles.css'
import {NavDropdown, MenuItem} from 'react-bootstrap';

class Topbar extends React.Component {
    constructor() {
        super();
        this.handleClick = this.handleClick.bind(this);
        this.search = this.search.bind(this);
        this.state = {
            redirectTo: false
        };
    }

    handleClick(event) {
        switch (event.target.id) {
            case "contact_us":
                event.preventDefault();
                break;
            case "about_us":
                event.preventDefault();
                break;
        }
    }

    search(event) {
        if (event.key === 'Enter') {
            if (this.props.location.pathname !== '/Search_results') {
                this.props.history.push({
                    pathname: '/Search_results',
                    searchQuery: event.target.value
                });
            } else {
                this.props.history.push({
                    searchQuery: event.target.value
                });
            }
        }
    }

    render() {
    return (
		<Navbar fluid inverse id="topbar">
            <Navbar.Header>
                <Navbar.Toggle />
            </Navbar.Header>
            <Navbar.Collapse>
                <Nav id="topbar-menu">
                    <NavDropdown noCaret title={M.products} className="topbar-item">
                        <MenuItem>
                            <Dropdown />
                        </MenuItem>
                    </NavDropdown>
                    <NavItem className="topbar-item"><Link to="/Design_gallery">{M.gallery}</Link></NavItem>
                    <NavDropdown noCaret title={M.service} className="topbar-item">
                        <MenuItem>
                            <ServiceDropdown/>
                        </MenuItem>
                    </NavDropdown>
                    <NavDropdown noCaret className="topbar-item" title={M.information}>
                        <MenuItem>
                            <InformationDropdown />
                        </MenuItem>
                    </NavDropdown>
                    <NavItem className="topbar-item"><Link to="/About_us">{M.about_us}</Link></NavItem>
                    <NavItem className="topbar-item"><Link to="/Contact_us">{M.contact_us}</Link></NavItem>
                    <NavItem className="topbar-item">
                        <i id="shopping-cart" className="fa fa-shopping-cart" aria-hidden="true"/>
                        <Link id="shopping-cart-text" to="/Shopping_cart">{M.shopping_cart}</Link>
                    </NavItem>
                    <NavItem className="topbar-item">
                        <input id="search-input" onKeyPress={this.search} placeholder={M.search}/>
                    </NavItem>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
		);
    }
}

export default withRouter(Topbar);

