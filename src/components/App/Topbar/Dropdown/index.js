/**
 * Created by gevorg on 8/8/17.
 */
import React from 'react';
import {Link} from 'react-router-dom';
import {ControlLabel} from 'react-bootstrap';
import M from '../../../../Messages/en.messages';
import './styles.css'

class Dropdown extends React.Component {
    render() {
        return (
            <div>
                <h3 id='dropdown-categories-title'>{M.categories}</h3>
                <div id="dropdown-categories">
                    <div id="block1">
                        <Link to="/Categories/Appliques"><ControlLabel
                            className="dropdown-item">{M.appliques}</ControlLabel></Link>
                        <Link to="/Categories/Balusters"><ControlLabel
                            className="dropdown-item">{M.balusters}</ControlLabel></Link>
                        <Link to="/Categories/Brackets"><ControlLabel
                            className="dropdown-item">{M.brackets}</ControlLabel></Link>
                        <Link to="/Categories/Capitals"><ControlLabel
                            className="dropdown-item">{M.capitals}</ControlLabel></Link>
                        <Link to="/Categories/Pillars"><ControlLabel
                            className="dropdown-item">{M.pillars}</ControlLabel></Link>
                        <Link to="/Categories/Bars"><ControlLabel
                            className="dropdown-item">{M.bars}</ControlLabel></Link>
                        {/*<Link to="/Categories/Arches"><ControlLabel className="dropdown-item">{M.arches}</ControlLabel></Link>*/}
                        {/*<Link to="/Categories/Baguettes"><ControlLabel*/}
                            {/*className="dropdown-item">{M.baguettes}</ControlLabel></Link>*/}
                        {/*<Link to="/Categories/Basreliefs"><ControlLabel*/}
                            {/*className="dropdown-item">{M.bas_reliefs}</ControlLabel></Link>*/}
                        {/*<Link to="/Categories/Mascarons"><ControlLabel*/}
                            {/*className="dropdown-item">{M.mascarons}</ControlLabel></Link>*/}
                    </div>
                    <div id="block2">
                        <Link to="/Categories/Cornices"><ControlLabel
                            className="dropdown-item">{M.cornices}</ControlLabel></Link>
                        <Link to="/Categories/Legs"><ControlLabel
                            className="dropdown-item">{M.legs}</ControlLabel></Link>
                        <Link to="/Categories/Pilasters"><ControlLabel
                            className="dropdown-item">{M.pilasters}</ControlLabel></Link>
                        <Link to="/Categories/Mouldings"><ControlLabel
                            className="dropdown-item">{M.mouldings}</ControlLabel></Link>
                        <Link to="/Categories/Rosettes"><ControlLabel
                            className="dropdown-item">{M.rosettes}</ControlLabel></Link>
                        {/*<Link to="/Categories/Cartouche"><ControlLabel*/}
                            {/*className="dropdown-item">{M.cartouche}</ControlLabel></Link>*/}
                        {/*<Link to="/Categories/Doors"><ControlLabel
                            className="dropdown-item">{M.doors}</ControlLabel></Link>
                        <Link to="/Categories/Panels"><ControlLabel className="dropdown-item">{M.panels}</ControlLabel></Link>
                        <Link to="/Categories/Decor"><ControlLabel
                            className="dropdown-item">{M.decor}</ControlLabel></Link>
                        <Link to="/Categories/Framework"><ControlLabel
                            className="dropdown-item">{M.framework}</ControlLabel></Link>
                        <Link to="/Categories/Souvenirs"><ControlLabel
                            className="dropdown-item">{M.souvenirs}</ControlLabel></Link>
                        <Link to="/Categories/Portraits"><ControlLabel
                            className="dropdown-item">{M.portraits}</ControlLabel></Link>
                        <Link to="/Categories/Bars"><ControlLabel
                            className="dropdown-item">{M.bars}</ControlLabel></Link>*/}
                    </div>
                </div>
            </div>
        );
    }
}

export default Dropdown;

