/**
 * Created by gevorg on 8/12/17.
 */

import React from 'react';
import {Grid} from "react-bootstrap";

import Footer from "../App/Footer";
import TopbarMain from '../App/TopbarMain';
import Constants from "../../Constants";

import M from '../../Messages/en.messages';
import './styles.css';

class About extends React.Component {

    componentDidMount() {
        window.addEventListener("language", function(e) {
            this.setState({});
        }.bind(this), false);
    }

    render() {
        return (
            <div>
                <TopbarMain/>
                <Grid id="about-block" className="app">
                    <h3 className="col-sm-12" id="about-us-title">{M.about_us}</h3>
                    <img className="about-us-image" src={Constants.AWS_URL + 'logo.png'}/>
                    <h4 id="about-mission">{M.description}</h4>
                    <p className="about-text">{M.about_us_text_p1}<br/></p>
                    <p className="about-text">{M.about_us_text_p2}<br/></p>
                    <p className="about-text">{M.about_us_text_p3}<br/></p>
                </Grid>
                <Footer/>
            </div>
        );
    }

}

export default About;
