import React from 'react';
import Footer from "../App/Footer";
import TopbarMain from '../App/TopbarMain';
import './styles.css';
import {Grid} from "react-bootstrap";
import M from '../../Messages/en.messages';


class DesignGallery extends React.Component {

    componentDidMount() {
        window.addEventListener("language", function(e) {
            this.setState({});
        }.bind(this), false);
    }

    render() {
        let images = [
            'https://s-media-cache-ak0.pinimg.com/736x/9d/67/60/9d6760711b1ed1e675b3fc6dfdcc137e--wood-carving-patterns-carving-designs.jpg',
            'https://s-media-cache-ak0.pinimg.com/736x/9d/67/60/9d6760711b1ed1e675b3fc6dfdcc137e--wood-carving-patterns-carving-designs.jpg',
            'https://s-media-cache-ak0.pinimg.com/736x/9d/67/60/9d6760711b1ed1e675b3fc6dfdcc137e--wood-carving-patterns-carving-designs.jpg'];
        return (
            <div>
                <TopbarMain/>
                <h3 id="design-gallery-title">{M.design_gallery}</h3>
                <Grid className="app">
                    <ul id="design-gallery-list">
                        <li className="gallery-list-item">
                            <img className="design-gallery-item"  src={images[0]}/>
                            <span className="gallery-category-name">{M.interiors}</span>
                        </li>
                        <li className="gallery-list-item">
                            <img className="design-gallery-item"  src={images[1]}/>
                            <span className="gallery-category-name">{M.furniture}</span>
                        </li>
                        <li className="gallery-list-item">
                            <img className="design-gallery-item"  src={images[2]}/>
                            <span className="gallery-category-name">{M.doors_upper}</span>
                        </li>
                    </ul>
                </Grid>
                <Footer/>
            </div>
        );
    }
}

export default DesignGallery;