/**
 * Created by gevorg on 8/10/17.
 */
import React from 'react';
import { Link } from 'react-router-dom';
import {Button, Grid} from "react-bootstrap";

import {CircleLoader} from 'react-spinners';

import '../styles.css';

import Footer from "../../App/Footer";
import TopbarMain from "../../App/TopbarMain";


import RequestManager from "../../../ApiManager/RequestManager";
import M from '../../../Messages/en.messages';
import Constants from '../../../Constants';


class CategoryWrapper extends React.Component {
    constructor() {
        super();
        this.state = {
            files: [],
            imagesState: 1
        };
        this.responseCallback = this.responseCallback.bind(this);
    }

    componentDidMount() {
        window.addEventListener("language", function(e) {
            this.setState({});
        }.bind(this), false);
    }

    responseCallback(response) {
        this.setState({
            files: response.result
        });
    }

    loadMoreComponents() {
        this.setState({
            imagesState: this.state.imagesState + 1
        })
    }

    componentWillMount() {
        RequestManager.makeRequest('getCategoryImages', this.props.match.params.category.toLowerCase(), this.responseCallback);
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.match.params.category.toLowerCase() !== nextProps.match.params.category.toLowerCase()) {
            RequestManager.makeRequest('getCategoryImages', nextProps.match.params.category.toLowerCase(), this.responseCallback);
        }
    }

    render() {
        let title = this.props.match.params.category.toLowerCase() + '_upper';
        let jsxImages = (
            <div className="loader-container">
                <CircleLoader color='black' loading={true} size={120}/>
                <span className="loader-span">Loading...</span>
            </div>
        );
		let isLoadButtonActive = true;
        if (this.state.files.length) {
            let images = [];
            for (let i = 0, len = (this.state.imagesState * 16); i < len; ++i) {
                if (!this.state.files[i]) {
					isLoadButtonActive = false;
                    break;
                }
                let source = Constants.AWS_URL + this.state.files[i].pathcol,
                    order = "/Order/" + this.state.files[i].codecol;
                images.push(
                    <Link to={order}>
                        <li className="list-item">
                            <img className="category-list-item" src={source}/>
                            <div className="category-info-container">
                                <span className="category-item-code">{this.state.files[i].codecol}</span>
                            </div>
                        </li>
                    </Link>
                );
            }
            jsxImages = (
                <ul id="category-list">
                    {images}
                </ul>
            );
        }

        return (
            <div>
                <TopbarMain/>
                <Grid id="category" className="app">
                    <h3 id="design-gallery-title">{M[title]}</h3>
                        {jsxImages}
                    {this.state.files.length > 16 && isLoadButtonActive ? <Button className="load-button" onClick={this.loadMoreComponents.bind(this)}>{M.load_more}</Button> : null}
                </Grid>
                <Footer/>
            </div>
        );
    }
}

export default CategoryWrapper;
